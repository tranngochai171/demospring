package com.myclass.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.myclass.util.PathConstants;
import com.myclass.util.UrlConstants;

@Controller
@RequestMapping(value = UrlConstants.URL_ERROR)
public class ErrorController {

	@RequestMapping(value = UrlConstants.URL_ERROR_404)
	public String pageNotFound() {
		return PathConstants.VIEW_ERROR_404;
	}

	@RequestMapping(value = UrlConstants.URL_ERROR_400)
	public String notValidParam() {
		return PathConstants.VIEW_ERROR_400;
	}
}
