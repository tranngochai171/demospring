package com.myclass.controller;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.myclass.entity.User;
import com.myclass.repository.UserRepository;
import com.myclass.util.PathConstants;
import com.myclass.util.UrlConstants;

@Controller
@RequestMapping(value = UrlConstants.URL_LOGIN)
public class LoginController {
	@Autowired
	private UserRepository userRepositoryl;

	@RequestMapping(value = "", method = RequestMethod.GET)
	public String index(ModelMap model) {
		model.addAttribute("user", new User());
		return PathConstants.VIEW_LOGIN;
	}

	@RequestMapping(value = "", method = RequestMethod.POST)
	public String index(@ModelAttribute("user") User user, ModelMap model) {
		User user_find = this.userRepositoryl.findByEmail(user.getEmail());
		if (user_find != null && BCrypt.checkpw(user.getPassword(), user_find.getPassword()))
			return "redirect:" + UrlConstants.URL_USER;
		else {
			model.addAttribute("message", "(*) Thông tin tài khoản chưa chính xác");
			return PathConstants.VIEW_LOGIN;
		}
	}
}
