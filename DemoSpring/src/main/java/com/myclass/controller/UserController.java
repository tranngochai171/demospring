package com.myclass.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.myclass.dto.UserChangePassWord;
import com.myclass.entity.User;
import com.myclass.repository.RoleRepository;
import com.myclass.repository.UserRepository;
import com.myclass.service.UserService;
import com.myclass.util.PathConstants;
import com.myclass.util.UrlConstants;

@Controller
@RequestMapping(value = UrlConstants.URL_USER)
public class UserController {
	@Autowired
	UserRepository userRepository;
	@Autowired
	UserService userService;
	@Autowired
	RoleRepository roleRepository;

	@RequestMapping(value = "", method = RequestMethod.GET)
	public String index(ModelMap model) {
		model.addAttribute("listUserDTO", this.userRepository.getListUserDTO());
		return PathConstants.VIEW_USER;
	}

	@RequestMapping(value = UrlConstants.URL_DELETE, method = RequestMethod.GET)
	public String index(@RequestParam("id") int id) {
		this.userRepository.deleteById(id);
		return UrlConstants.REDIRECT + UrlConstants.URL_USER;
	}

	@RequestMapping(value = UrlConstants.URL_ADD, method = RequestMethod.GET)
	public String add(ModelMap model) {
		model.addAttribute("user", new User());
		model.addAttribute("listRole", this.roleRepository.getListRole());
		return PathConstants.VIEW_USER_ADD;
	}

	@RequestMapping(value = UrlConstants.URL_ADD, method = RequestMethod.POST)
	public String add(@ModelAttribute("user") User user, ModelMap model) {
		if (this.userRepository.addNewUser(user) <= 0) {
			model.addAttribute("message", "Thêm mới thất bại");
			return PathConstants.VIEW_USER_ADD;
		}
		return UrlConstants.REDIRECT + UrlConstants.URL_USER;
	}

	@RequestMapping(value = UrlConstants.URL_EDIT_INF, method = RequestMethod.GET)
	public String editInf(ModelMap model, @RequestParam("id") int id) {
		model.addAttribute("user", this.userRepository.findById(id));
		model.addAttribute("listRole", this.roleRepository.getListRole());
		return PathConstants.VIEW_USER_EDIT_INF;
	}

	@RequestMapping(value = UrlConstants.URL_EDIT_INF, method = RequestMethod.POST)
	public String editInf(ModelMap model, @ModelAttribute("user") User user) {
		if (this.userRepository.editInforUser(user) <= 0) {
			model.addAttribute("message", "(*) Cập nhật thông tin thất bại");
			model.addAttribute("user", this.userRepository.findById(user.getId()));
			return PathConstants.VIEW_USER_EDIT_INF;
		}
		return UrlConstants.REDIRECT + UrlConstants.URL_USER;
	}

	@RequestMapping(value = UrlConstants.URL_EDIT_PASS, method = RequestMethod.GET)
	public String editPass(ModelMap model, @RequestParam("id") int id) {
		model.addAttribute("user", this.userRepository.findByIdForEditPass(id));
		return PathConstants.VIEW_USER_EDIT_PASS;
	}

	@RequestMapping(value = UrlConstants.URL_EDIT_PASS, method = RequestMethod.POST)
	public String editPass(@ModelAttribute("user") UserChangePassWord user_change, ModelMap model) {
		User user = this.userRepository.findById(user_change.getId());
		System.out.println(user.getPassword());
		System.out.println(user_change.getOld_password() + " " + user_change.getNew_password());
		boolean success = true;
		if (!this.userService.validOldPassword(user.getPassword(), user_change.getOld_password())) {
			model.addAttribute("message", "(*) Mật khẩu cũ chưa chính xác");
			success = false;
		} else if (!this.userService.validNewPassword(user_change.getOld_password(), user_change.getNew_password())) {
			model.addAttribute("message", "(*) Mật khẩu cũ trùng với mật khẩu mới");
			success = false;
		} else if (this.userRepository.editPassUser(user_change) <= 0) {
			model.addAttribute("message", "(*) Cập nhật mật khẩu thất bại");
			success = false;
		}
		if (success)
			return UrlConstants.REDIRECT + UrlConstants.URL_USER;
		model.addAttribute("user", user_change);
		return PathConstants.VIEW_USER_EDIT_PASS;
	}

}
