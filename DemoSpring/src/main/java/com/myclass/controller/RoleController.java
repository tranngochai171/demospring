package com.myclass.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.myclass.entity.Role;
import com.myclass.repository.RoleRepository;
import com.myclass.util.PathConstants;
import com.myclass.util.UrlConstants;

@Controller
@RequestMapping(value = UrlConstants.URL_ROLE)
public class RoleController {
	@Autowired
	RoleRepository roleRepository;

	@RequestMapping(value = "", method = RequestMethod.GET)
	public String index(ModelMap model) {
		List<Role> listRole = this.roleRepository.getListRole();
		model.addAttribute("listRole", listRole);
		return PathConstants.VIEW_ROLE;
	}

	@RequestMapping(value = UrlConstants.URL_DELETE, method = RequestMethod.GET)
	public String delete(@RequestParam("id") int id) {
		this.roleRepository.deleteRoleById(id);
		return UrlConstants.REDIRECT + UrlConstants.URL_ROLE;
	}

	@RequestMapping(value = UrlConstants.URL_ADD, method = RequestMethod.GET)
	public String add(ModelMap model) {
		model.addAttribute("role", new Role());
		return PathConstants.VIEW_ROLE_ADD;
	}

	@RequestMapping(value = UrlConstants.URL_ADD, method = RequestMethod.POST)
	public String add(@ModelAttribute("role") Role role, ModelMap model, BindingResult error) {
		if (role.getName().trim().length() == 0) {
			error.rejectValue("name", "role", "(*) Vui lòng nhập tên");
		}
		if (role.getDescription().trim().length() == 0) {
			error.rejectValue("description", "role", "(*) Vui lòng nhập thông tin");
		}
		if (error.hasErrors() || this.roleRepository.addNewRole(role) <= 0) {
			model.addAttribute("message", "(*) Thêm mới role thất bại");
			return PathConstants.VIEW_ROLE_ADD;
		}
		return UrlConstants.REDIRECT + UrlConstants.URL_ROLE;
	}

	@RequestMapping(value = UrlConstants.URL_EDIT, method = RequestMethod.GET)
	public String edit(@RequestParam("id") int id, ModelMap model) {
		Role role = this.roleRepository.findById(id);
		model.addAttribute("role", role);
		return PathConstants.VIEW_ROLE_EDIT;
	}

	@RequestMapping(value = UrlConstants.URL_EDIT, method = RequestMethod.POST)
	public String edit(@ModelAttribute("role") Role role, ModelMap model) {
		if (this.roleRepository.editRole(role) <= 0) {
			model.addAttribute("message", "Thêm mới role thất bại");
			return PathConstants.VIEW_ROLE_EDIT;
		}
		return UrlConstants.REDIRECT + UrlConstants.URL_ROLE;
	}
}
