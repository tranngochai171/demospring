package com.myclass.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JDBCConnection {
	final static String DATABASE = "jdbc:mysql://localhost:3306/demoSping";
	final static String USER_NAME = "root";
	final static String PASSWORD = "password";

	public static Connection getConnection() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			return DriverManager.getConnection(DATABASE, USER_NAME, PASSWORD);
		} catch (ClassNotFoundException e) {
			System.out.println("Không tìm thấy driver");
			e.printStackTrace();
		} catch (SQLException e) {
			System.out.println("Không thể kết nối database");
			e.printStackTrace();
		}
		return null;
	}
}
