package com.myclass.service;

public interface UserService {
	public boolean validOldPassword(String password, String old_password);
	public boolean validNewPassword(String password, String new_password);
}
