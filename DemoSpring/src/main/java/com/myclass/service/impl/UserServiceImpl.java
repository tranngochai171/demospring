package com.myclass.service.impl;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.stereotype.Service;

import com.myclass.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Override
	public boolean validOldPassword(String password, String old_password) {
		if (!BCrypt.checkpw(old_password, password))
			return false;
		return true;
	}

	@Override
	public boolean validNewPassword(String old_password, String new_password) {
		if (old_password.equals(new_password))
			return false;
		return true;
	}

}
