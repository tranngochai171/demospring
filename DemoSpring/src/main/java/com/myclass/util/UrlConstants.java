package com.myclass.util;

public class UrlConstants {
	// COMMON URL
	public static final String REDIRECT = "redirect:";
	public static final String URL_ADD = "/add";
	public static final String URL_DELETE = "/delete";
	public static final String URL_EDIT = "/edit";
	// ERROR CONTROLLER
	public static final String URL_ERROR = "/error";
	public static final String URL_ERROR_404 = "/404";
	public static final String URL_ERROR_400 = "/400";
	// LOGIN CONTROLLER
	public static final String URL_LOGIN = "/login";
	// ROLE CONTROLLER
	public static final String URL_ROLE = "/role";
	public static final String URL_ROLE_ADD = "/role/add";
	public static final String URL_ROLE_EDIT = "/role/edit";
	public static final String URL_ROLE_DELETE = "/role/delete";
	// USER CONTROLLER
	public static final String URL_USER = "/user";
	public static final String URL_USER_ADD = "/user/add";
	public static final String URL_USER_EDIT_INF = "/user/edit-inf";
	public static final String URL_EDIT_INF = "/edit-inf";
	public static final String URL_USER_EDIT_PASS = "/user/edit-pass";
	public static final String URL_EDIT_PASS = "/edit-pass";
	public static final String URL_USER_DELETE = "/user/delete";
}
