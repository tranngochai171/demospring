package com.myclass.util;

public class PathConstants {

	// ERROR VIEW
	public static final String VIEW_ERROR_404 = "error/404";
	public static final String VIEW_ERROR_400 = "error/400";
	// LOGIN VIEW
	public static final String VIEW_LOGIN = "login/index";
	// ROLE VIEW
	public static final String VIEW_ROLE = "role/index";
	public static final String VIEW_ROLE_ADD = "role/add";
	public static final String VIEW_ROLE_EDIT = "role/edit";
	// USER VIEW
	public static final String VIEW_USER = "user/index";
	public static final String VIEW_USER_ADD = "user/add";
	public static final String VIEW_USER_EDIT_INF = "user/edit-inf";
	public static final String VIEW_USER_EDIT_PASS = "user/edit-pass";
}
