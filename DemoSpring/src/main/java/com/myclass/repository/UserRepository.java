package com.myclass.repository;

import java.util.List;

import com.myclass.dto.UserChangePassWord;
import com.myclass.dto.UserDTO;
import com.myclass.entity.User;

public interface UserRepository {
	public User findByEmail(String email);

	public User findById(int id);

	public UserChangePassWord findByIdForEditPass(int id);

	public List<UserDTO> getListUserDTO();

	public int addNewUser(User user);

	public int deleteById(int id);

	public int editInforUser(User user);

	public int editPassUser(UserChangePassWord user);
}
