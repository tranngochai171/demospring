package com.myclass.repository;

import java.util.List;

import com.myclass.entity.Role;

public interface RoleRepository {
	public List<Role> getListRole();

	public int addNewRole(Role role);

	public int deleteRoleById(int id);

	public Role findById(int id);

	public int editRole(Role role);
}
