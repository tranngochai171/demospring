package com.myclass.repository.imlp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.myclass.connection.JDBCConnection;
import com.myclass.entity.Role;
import com.myclass.repository.RoleRepository;

@Repository
public class RoleRepositoryImpl implements RoleRepository {

	public List<Role> getListRole() {
		List<Role> listRole = new LinkedList<Role>();
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "SELECT * FROM roles;";
			PreparedStatement stm = conn.prepareStatement(query);
			ResultSet res = stm.executeQuery();
			while (res.next()) {
				Role role = new Role();
				role.setId(res.getInt("id"));
				role.setName(res.getString("name"));
				role.setDescription(res.getString("description"));
				listRole.add(role);
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listRole;
	}

	@Override
	public int addNewRole(Role role) {
		int result = 0;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "INSERT INTO roles (name,description) VALUES (?,?);";
			PreparedStatement stm = conn.prepareStatement(query);
			stm.setString(1, role.getName());
			stm.setString(2, role.getDescription());
			result = stm.executeUpdate();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public int deleteRoleById(int id) {
		int result = 0;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "DELETE FROM roles WHERE roles.id = ?;";
			PreparedStatement stm = conn.prepareStatement(query);
			stm.setInt(1, id);
			result = stm.executeUpdate();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public Role findById(int id) {
		Role role = null;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "SELECT * FROM roles WHERE roles.id = ?;";
			PreparedStatement stm = conn.prepareStatement(query);
			stm.setInt(1, id);
			ResultSet res = stm.executeQuery();
			if (res.next()) {
				role = new Role();
				role.setId(id);
				role.setName(res.getString("name"));
				role.setDescription(res.getString("description"));
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return role;
	}

	@Override
	public int editRole(Role role) {
		int result = 0;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "UPDATE roles SET name = ?, description = ? WHERE roles.id = ?;";
			PreparedStatement stm = conn.prepareStatement(query);
			stm.setString(1, role.getName());
			stm.setString(2, role.getDescription());
			stm.setInt(3, role.getId());
			result = stm.executeUpdate();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

}
