package com.myclass.repository.imlp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.stereotype.Repository;

import com.myclass.connection.JDBCConnection;
import com.myclass.dto.UserChangePassWord;
import com.myclass.dto.UserDTO;
import com.myclass.entity.User;
import com.myclass.repository.UserRepository;

@Repository
public class UserRepositoryImpl implements UserRepository {

	public User findByEmail(String email) {
		User user = null;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "SELECT * FROM users WHERE users.email = ?;";
			PreparedStatement stm = conn.prepareStatement(query);
			stm.setString(1, email);
			ResultSet res = stm.executeQuery();
			if (res.next()) {
				user = new User();
				user.setEmail(email);
				user.setFullname(res.getString("fullname"));
				user.setPassword(res.getString("password"));
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return user;
	}

	@Override
	public List<UserDTO> getListUserDTO() {
		List<UserDTO> listUserDTO = new LinkedList<UserDTO>();
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "SELECT * FROM users JOIN roles ON users.role_id = roles.id;";
			PreparedStatement stm = conn.prepareStatement(query);
			ResultSet res = stm.executeQuery();
			while (res.next()) {
				UserDTO userDTO = new UserDTO();
				userDTO.setId(res.getInt("id"));
				userDTO.setEmail(res.getString("email"));
				userDTO.setFullname(res.getString("fullname"));
				userDTO.setRole_id(res.getInt("role_id"));
				userDTO.setRole_name(res.getString("name"));
				listUserDTO.add(userDTO);
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listUserDTO;
	}

	@Override
	public int addNewUser(User user) {
		int result = 0;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "INSERT INTO users (email,password,fullname,role_id) VALUES (?,?,?,?);";
			PreparedStatement stm = conn.prepareStatement(query);
			stm.setString(1, user.getEmail());
			stm.setString(2, BCrypt.hashpw(user.getPassword(), BCrypt.gensalt(12)));
			stm.setString(3, user.getFullname());
			stm.setInt(4, user.getRole_id());
			result = stm.executeUpdate();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public int deleteById(int id) {
		int result = 0;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "DELETE FROM users WHERE users.id = ?;";
			PreparedStatement stm = conn.prepareStatement(query);
			stm.setInt(1, id);
			result = stm.executeUpdate();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public User findById(int id) {
		User user = null;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "SELECT * FROM users WHERE users.id = ?;";
			PreparedStatement stm = conn.prepareStatement(query);
			stm.setInt(1, id);
			ResultSet res = stm.executeQuery();
			if (res.next()) {
				user = new User();
				user.setId(id);
				user.setEmail(res.getString("email"));
				user.setFullname(res.getString("fullname"));
				user.setPassword(res.getString("password"));
				user.setRole_id(res.getInt("role_id"));
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return user;
	}

	@Override
	public UserChangePassWord findByIdForEditPass(int id) {
		UserChangePassWord user = null;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "SELECT * FROM users WHERE users.id = ?;";
			PreparedStatement stm = conn.prepareStatement(query);
			stm.setInt(1, id);
			ResultSet res = stm.executeQuery();
			if (res.next()) {
				user = new UserChangePassWord();
				user.setId(id);
				user.setEmail(res.getString("email"));
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return user;
	}

	@Override
	public int editInforUser(User user) {
		int result = 0;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "UPDATE users SET fullname = ?, role_id = ? WHERE users.id = ?;";
			PreparedStatement stm = conn.prepareStatement(query);
			stm.setString(1, user.getFullname());
			stm.setInt(2, user.getRole_id());
			stm.setInt(3, user.getId());
			result = stm.executeUpdate();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public int editPassUser(UserChangePassWord user) {
		int result = 0;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "UPDATE users SET password = ? WHERE users.id = ?;";
			PreparedStatement stm = conn.prepareStatement(query);
			stm.setString(1, BCrypt.hashpw(user.getNew_password(), BCrypt.gensalt(12)));
			stm.setInt(2, user.getId());
			result = stm.executeUpdate();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

}
