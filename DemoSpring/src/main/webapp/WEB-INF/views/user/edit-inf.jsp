<%@page import="com.myclass.util.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Bootstrap Example</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>
<body>

	<div class="container mt-4">
		<div class="row">
			<div class="col-md-6 m-auto">
				<h2>Cập nhật người dùng</h2>
				<p class="text-center text-warning">${message }</p>
				<form:form action="edit-inf" modelAttribute="user" method="POST">
					<form:hidden path="id" />
					<div class="form-group">
						<label>Email</label>
						<form:input readonly="true" path="email" cssClass="form-control" />
					</div>
					<%-- <div class="form-group">
						<label>Password</label>
						<form:password path="password" cssClass="form-control" />
					</div> --%>
					<div class="form-group">
						<label>Fullname</label>
						<form:input path="fullname" class="form-control" />
					</div>
					<div class="form-group">
						<label>Quyền</label>
						<form:select path="role_id" cssClass="form-control"
							items="${listRole }" itemLabel="name" itemValue="id" />
					</div>
					<!-- <div class="form-group">
						<label>Quyền</label> <select class="form-control">
							<option>Quản trị</option>
							<option>Nhân viên</option>
						</select>
					</div> -->
					<button type="submit" class="btn btn-primary mt-3">Submit</button>
					<a href='<c:url value="<%=UrlConstants.URL_USER%>"/>'
						class="btn btn-secondary">Quay lại</a>
				</form:form>
			</div>
		</div>
	</div>

</body>
</html>