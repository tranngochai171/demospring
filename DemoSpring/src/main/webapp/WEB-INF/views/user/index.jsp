<%@page import="com.myclass.util.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Bootstrap Example</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
</head>
<body>

	<div class="container mt-4">
		<h2>Danh sách Nhân sự</h2>
		<a href='<c:url value="<%=UrlConstants.URL_USER_ADD%>"/>'
			class="btn btn-sm btn-success my-3">Thêm mới</a>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>Id</th>
					<th>Fullname</th>
					<th>Email</th>
					<th>Role</th>
					<th>#</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="item" items="${listUserDTO }">
					<tr>
						<td>${item.id }</td>
						<td>${item.fullname }</td>
						<td>${item.email }</td>
						<td>${item.role_name }</td>
						<td><a
							href='<c:url value="<%=UrlConstants.URL_USER_EDIT_INF%>"/>?id=${item.id}'
							class="btn btn-sm btn-success">Sửa thông tin</a><a
							href='<c:url value="<%=UrlConstants.URL_USER_EDIT_PASS%>"/>?id=${item.id}'
							class="btn btn-sm btn-warning">Sửa mật khẩu</a> <a
							href='<c:url value="<%=UrlConstants.URL_USER_DELETE%>"/>?id=${item.id}'
							class="btn btn-sm btn-danger">Xóa</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>

	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</body>
</html>