<%@page import="com.myclass.util.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en">

<head>
<title>Bootstrap Example</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>

<body>

	<div class="container mt-4">
		<div class="row">
			<div class="col-md-6 m-auto">
				<h2>Thêm mới quyền</h2>
				<p class="text-center text-warning">${message }</p>
				<form:form action="add" method="POST" modelAttribute="role">
					<div class="form-group">
						<label>Name</label>
						<form:input cssClass="form-control" path="name" />
						<form:errors cssClass="text-danger" path="name" />
					</div>
					<div class="form-group">
						<label>Description</label>
						<form:input class="form-control" path="description" />
						<form:errors cssClass="text-danger" path="description" />
					</div>
					<button type="submit" class="btn btn-primary">Submit</button>
					<a href='<c:url value="<%=UrlConstants.URL_ROLE%>"/>'
						class="btn btn-secondary">Quay lại</a>
				</form:form>
			</div>
		</div>
	</div>

</body>

</html>